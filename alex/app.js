const express = require('express')
const app = express()
const async = require('async')
const sqlite3 = require('sqlite3')
const _ = require('lodash')
const cors = require('cors')

let db = null

// function connectAndQuery(string, callback) {
// let dbUrl = '/Users/alexsatur/Desktop/medicSpot/sqldite/locationData.db'
//   let fns = []
//   fns.push(done =>{
//     if(db){
//       done()
//     } else {
//       db = new sqlite3.Database(dbUrl, (err) =>{
//         done(err)
//       })
//     }
//   })
//   fns.push(done =>{
//     let queryString = `SELECT name FROM locations WHERE name LIKE '%${string}%'`
//   })
//   async.series(fns, (err, resp) =>{
//     if(err){
//       callback(err)
//     } else {
//       callback(resp)
//     }
//   })
// }

function connect(callback) {
  if (db) {
    console.log('Already Connected.')
    callback()
  } else {
    console.log('Connecting')
    db = new sqlite3.Database(__dirname + '/sqlite/locationData.db', (err) => {
      callback(err)
    })
  }
}

function query(string, options, callback) {
  if(_.isFunction(options)) {
    callback = options
    options = {}
  }
  let tableName = options.tableName || 'locations'
  let queryString = `SELECT name, latitude, longitude FROM locations WHERE name LIKE '%${string}%' ORDER BY name`
  db.all(queryString, {}, (err, rows) => {
    if (err) {
      callback(err)
    } else {
      callback(null, rows)
    }
  })
}

function closeConnection() {
  db.close()
}

app.use(cors())

app.use(express.static('public'))

app.get('/home', (req, res) => {
  res.sendFile(__dirname + '/views/index.html');
})

app.get('/locations', (req, res) => {
  let fns = []
  fns.push(done => {
    connect((err) => {
      done(err)
    })
  })
  fns.push(done => {
    let queryString = _.get(req, 'query.q', '')
    // Add some checking here if this query is missing
    query(queryString, (err, resp) => {
      locations = resp
      done(err, resp)
    })
  })
  async.series(fns, (err) => {
    if (err) {
      res.json(err)
    } else {
      res.json(locations)
    }
  })
})

const server = app.listen('8080', () => {
  console.log('Serving...')
})

module.exports = server
