# Node code challenge

## Part 1: Questions

Qs1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```
When the program runs, any code inside the set timeout function will be set aside until its set time to execute comes around, so in this case you would see '2' then 100ms later '1' logged output

Qs2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```
The first time the function foo is called with d = 0, as this is less than 10 it will pass this number plus one back into the function, this continues until d = 10, at which point it will not trigger the if, but log out 10, then 9, 8, 7 etc as the other instances of foo being called complete

Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```
If setting the value to default to five, if you were to pass 0 into, this would be overridden to 5 which may not be desired behaviour. Also defaulting to 5 implies this needs to be a number, but if something other than a number, a function or object were to somehow make its way in here it would not be changed, potentially causing problems further down.

Qs4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```

bar = foo(1) is now a function where the variable a has been set at 1, so calling bar with the value 2 will console log out 1 + 2 = 3

Qs5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

The function double takes two arguments, a and a function done. It will wait 100ms before resolving done with double the value provided. This could be used in cases where you're waiting for an third party api or db to complete, if the 100 were replaced with the value of a it would double the length of time before trying again to see if something has finished.

## Part 2 and 3

##How to Run

1. Clone this repo
2. Change into the repo and run 'npm install'
3. Run 'nodemon app.js'
4. Navigate to localhost:8080/home and start typing a location into the search bar
5. To run tests 'npm test'

###Tech Used

I chose an express server and jquery for the front end. The reason behind this is I am familiar with both technologies and given the time allowance and size of the project thought these would be suitable


## Things to Improve With Time

- **Ordering**
I've also just noticed that ORDER by name is not working exactly how i would like, this this should be fixed

- **Error handling**
i think erorr handling could be better done to really give clear messages back to the user and allow them to debug

- **Search function**
The query string is hard coded which feels restrictive, in future it might be nice to have multiple routes for different searches for example.

- **Testing**
With more time I would like to write more tests and follow the entire project through with a test driven approach.

- **Styling**
The CSS and HTML is very basic and could be made much more attractive and pleasing to use.

- **This readme**
Could be much better!
