const mocha = require('mocha')
const should = require('should')
const request = require('supertest')

const app = require('../app')

describe('location searching', () => {
  it('will welcome the user', (done) => {
    request(app).get('/home')
      // .expect(200)
      .expect(/Welcome!/, done)
  })
  it('can handle a location search', (done) => {
    request(app).get('/locations?q=sheffield')
    .expect('Content-Type', /json/)
    done()
  })
  it('will not let numbers be searched', done =>{
    done()
  })
})
