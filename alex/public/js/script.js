$(function() {

  $("#searchBar").keyup(function() {
    let string = $(this).val().replace(/[^A-Za-z]+/g, '') // some locations have numbers in
    if (string.length >= 2) {
      let httpApi = 'http://localhost:8080/locations?q=' + string
      $.get(httpApi, (res) => {
        processResponse(res)
      })
    } else if (!string.length) {
      $('#locationHolder').html('')
    }
  })

  function processResponse(response) {
    let newHtml = ''
    if (response.errno) {
      newHtml = processError(response)
    } else {
      response.forEach(function(location) {
        newHtml += `<div class='location'>${location.name} (${location.latitude}, ${location.longitude})</div>`
      })
    }
    $('#locationHolder').html(newHtml)
  }

  function processError(error) {
    let errCode = error.errno
    switch (errCode) {
      // TODO: Add others
      case 1:
        return `Generic error, error code: ${errCode}`;
      case 14:
        return `Unable to open the database file, error code: ${errCode}`;
      default:
        return 'Unknown Error';
    }
  }
})
